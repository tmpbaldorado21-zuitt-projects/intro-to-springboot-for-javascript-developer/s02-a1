package com.s02.a1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S02A1Application {

	public static void main(String[] args) {
		SpringApplication.run(S02A1Application.class, args);
	}

}
