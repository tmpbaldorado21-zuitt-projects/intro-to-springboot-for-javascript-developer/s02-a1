package com.s02.a1.models;

import javax.persistence.*;

@Table(name = "users")
public class users {

    //Properties
    @Id                 //primary key
    @GeneratedValue     //auto-increment
    private Long id;

    @Column             //field
    private String username;

    @Column             //field
    private String password;

    //Constructors
    public users() {}

    public users(String username, String password){
        this.username = username;
        this.password = password;
    }

    //Getters and Setters


    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() { return password; }

    public String getPassword(String password) {
        return password;
    }
}
