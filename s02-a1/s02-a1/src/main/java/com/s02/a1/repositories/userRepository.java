package com.s02.a1.repositories;

import com.s02.a1.models.users;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;


@Repository
public interface userRepository extends CrudRepository<users, Object> {

}